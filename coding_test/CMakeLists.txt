#参考：https://blog.csdn.net/new9232/article/details/121374943
#指定版本
cmake_minimum_required(VERSION 3.16)

#指定工程名称
project(coding_test LANGUAGES C CXX ASM)

#工程根目录(即为CMakeLists.txt文件所在目录)
message("PROJECT_SOURCE_DIR : " ${PROJECT_SOURCE_DIR})
#编译目录(即为执行cmake的目录)
message("PROJECT_BINARY_DIR : " ${PROJECT_BINARY_DIR})
#当前目录
message("CMAKE_CURRENT_SOURCE_DIR : " ${CMAKE_CURRENT_SOURCE_DIR})

#cmake macro
message("3.可以使用一些预定义的变量访问可选参数:ARGC, ARGV, ARGN")
macro(name_list name1 name2)
    message("argument count: ${ARGC}")
    message("all arguments: ${ARGV}")
    message("optional arguments: ${ARGN}")
endmacro()

name_list(Jack Kate Jony Tom)
name_list(Jack Kate)

## loop curdir's subdir
macro(SUBDIRLISTINCLUDE result curdir)
    set(dirlist "")
    file(GLOB_RECURSE DIRS_WITH_HEADERS "${curdir}/*.h")
    foreach(header ${DIRS_WITH_HEADERS})
        message("header: ${header}")
        get_filename_component(dir ${header} DIRECTORY)
        message("dir: ${dir}")
        list(APPEND dirlist ${dir})
    endforeach()
    list(REMOVE_DUPLICATES dirlist)
    set(${result} ${dirlist})
endmacro()

set(PRJ_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

SUBDIRLISTINCLUDE(PRJ_H_FOLDERS ${CMAKE_CURRENT_SOURCE_DIR}/include)
SUBDIRLISTINCLUDE(SRC_H_FOLDERS ${SRC_DIR})

set(SRC_MAIN ${SRC_DIR}/main.c)
file(GLOB_RECURSE APP_FILE ${SRC_DIR}/app/*.c)
file(GLOB_RECURSE HAL_FILE ${SRC_DIR}/hal/*.c)
file(GLOB_RECURSE DFX_FILE ${SRC_DIR}/dfx/*.c)
file(GLOB_RECURSE RAID_FILE ${SRC_DIR}/raid/*.c)

list(APPEND CODING_TEST_SRCS    ${SRC_MAIN}
                                ${APP_FILE}
                                ${HAL_FILE}
                                ${DFX_FILE}
                                ${RAID_FILE}
                                )

set(CODING_TEST_SRCS_FOR_DT ${CODING_TEST_SRCS})
list(REMOVE_ITEM CODING_TEST_SRCS_FOR_DT  ${SRC_MAIN})

if(CMAKE_TEST_DT_BASE)
    message("enter dt")
    add_subdirectory(dt_test)
else()
    message("enter src")
    add_subdirectory(src)
endif()