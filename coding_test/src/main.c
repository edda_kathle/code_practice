#include <stdio.h>
#include "helloworld.h"
#include "raid5.h"

// typedef __u64 __bitwise __le64;


int main(void)
{
    hello_world();

    // printf("sizeof(__le64 *):%d\n", sizeof(__le64 *));

    run_raid5_sample_code();

    return 0;
}
