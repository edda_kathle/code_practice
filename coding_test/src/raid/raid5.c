#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "raid5.h"

#define NUM_DISKS 3
#define BLOCK_SIZE 512
#define NUM_BLOCKS 10  // Number of data blocks
#define PARITY_BLOCK_INDEX (NUM_DISKS - 1)

typedef struct Disk {
    int id;
    char* data;
} Disk;

Disk disks[NUM_DISKS];

// Initialize disks
void initializeDisks() {
    for (int i = 0; i < NUM_DISKS; i++) {
        disks[i].id = i;
        disks[i].data = (char*)malloc(BLOCK_SIZE * NUM_BLOCKS);
        // Initialize data to zeros
        for (int j = 0; j < BLOCK_SIZE * NUM_BLOCKS; j++) {
            disks[i].data[j] = 0;
        }
    }
}

// Write data to RAID 5
void writeToRAID5(int blockIndex, char* data) {
    // Write data to data disks
    for (int i = 0; i < PARITY_BLOCK_INDEX; i++) {
        memcpy(disks[i].data + blockIndex * BLOCK_SIZE, data, BLOCK_SIZE);
    }

    // Calculate and write parity
    char parity = 0;
    for (int i = 0; i < PARITY_BLOCK_INDEX; i++) {
        parity ^= disks[i].data[blockIndex * BLOCK_SIZE];
    }
    disks[PARITY_BLOCK_INDEX].data[blockIndex * BLOCK_SIZE] = parity;
}

// Read data from RAID 5
void readFromRAID5(int blockIndex, char* result) {
    // Read data from data disks
    for (int i = 0; i < PARITY_BLOCK_INDEX; i++) {
        memcpy(result, disks[i].data + blockIndex * BLOCK_SIZE, BLOCK_SIZE);
    }

    // Calculate and verify parity
    char parity = 0;
    for (int i = 0; i < PARITY_BLOCK_INDEX; i++) {
        parity ^= disks[i].data[blockIndex * BLOCK_SIZE];
    }
    if (parity != disks[PARITY_BLOCK_INDEX].data[blockIndex * BLOCK_SIZE]) {
        printf("Parity error: Data corrupted.\n");
    }
}

// Cleanup resources
void cleanup() {
    for (int i = 0; i < NUM_DISKS; i++) {
        free(disks[i].data);
    }
}

void xor_8regs_2(unsigned long bytes, unsigned long *p1, unsigned long *p2)
{
	long lines = bytes / (sizeof (long)) / 8;

	do {
		p1[0] ^= p2[0];
		p1[1] ^= p2[1];
		p1[2] ^= p2[2];
		p1[3] ^= p2[3];
		p1[4] ^= p2[4];
		p1[5] ^= p2[5];
		p1[6] ^= p2[6];
		p1[7] ^= p2[7];
		p1 += 8;
		p2 += 8;
	} while (--lines > 0);
}

// int main()
int32_t run_raid5_sample_code(void)
{
    initializeDisks();

    // Example: Writing data
    char dataToWrite[BLOCK_SIZE] = "Hello, RAID 5!";
    writeToRAID5(0, dataToWrite);

    // Example: Reading data
    char readData[BLOCK_SIZE];
    readFromRAID5(0, readData);
    printf("Read data: %s\n", readData);

    cleanup();
    return 0;
}
