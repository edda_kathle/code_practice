#CODING_TEST_SRCS变量相关的内容移到了上一级的CMakeLists.txt,否则dt_test目录的CMakeLists.txt看不到平级的CMakeLists.txt的变量
#dt_test的CMakeLists.txt需要用CODING_TEST_SRCS_FOR_DT和${PRJ_H_FOLDERS} ${SRC_H_FOLDERS}
# set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR})

# # SUBDIRLISTINCLUDE(LIB_H_FOLDERS ${SRC_DIR}/lib)
# # SUBDIRLISTINCLUDE(APP_H_FOLDERS ${SRC_DIR}/app)
# # SUBDIRLISTINCLUDE(HAL_H_FOLDERS ${SRC_DIR}/hal)
# # SUBDIRLISTINCLUDE(DFX_H_FOLDERS ${SRC_DIR}/dfx)
# # SUBDIRLISTINCLUDE(RAID_H_FOLDERS ${SRC_DIR}/raid)
# SUBDIRLISTINCLUDE(SRC_H_FOLDERS ${SRC_DIR})

# set(SRC_MAIN ${SRC_DIR}/main.c)
# file(GLOB_RECURSE APP_FILE ${SRC_DIR}/app/*.c)
# file(GLOB_RECURSE HAL_FILE ${SRC_DIR}/hal/*.c)
# file(GLOB_RECURSE DFX_FILE ${SRC_DIR}/dfx/*.c)
# file(GLOB_RECURSE RAID_FILE ${SRC_DIR}/raid/*.c)

# list(APPEND CODING_TEST_SRCS    ${SRC_MAIN}
#                                 ${APP_FILE}
#                                 ${HAL_FILE}
#                                 ${DFX_FILE}
#                                 ${RAID_FILE}
#                                 )

set(CODING_TEST_TARGET "coding_test_main")
add_executable( ${CODING_TEST_TARGET}  ${CODING_TEST_SRCS})

# target_include_directories(${CODING_TEST_TARGET} PUBLIC ${PRJ_H_FOLDERS} ${APP_H_FOLDERS} ${HAL_H_FOLDERS} ${DFX_H_FOLDERS})
target_include_directories(${CODING_TEST_TARGET} PUBLIC ${PRJ_H_FOLDERS} ${SRC_H_FOLDERS})

# copy compile_commands.json to root workspace
# add_custom_command(
#     TARGET ${CODING_TEST_TARGET} POST_BUILD
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/compile_commands.json ${PRJ_ROOT_DIR}/.
#     VERBATIM)