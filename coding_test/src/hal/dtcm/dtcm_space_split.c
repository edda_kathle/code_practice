#include <stdint.h>
#include <stdio.h>

typedef struct {
    int32_t *addr;
    int32_t *max_size;
} dtcm_item;

enum dtcm_item_list {
    DTCM_ITEM_X,
    DTCM_ITEM_Y,
    DTCM_ITEM_Z,
    DTCM_ITEM_MAX
};

/*
方案一：
需要一个所有item的size的列表，跟据这个固定顺序的size的列表，以DTCM的BASE为基址，
按dtcm_item_list定义的顺序一次初始化g_dtcm_item_list中每个item的*addr、size。
下电的时候存一个副本到NAND中，并记录存NAND的地址到NOR，重新上电时，将数据读回DTCM
方案二：
在下电时，依次调用所有模块的下电接口，传入一个addr和max_size，给对应的模块写入
需要存盘的数据，接口出参为实际写入的数据size。最后将各个模块写入的size也存盘
*/
dtcm_item g_dtcm_item_list[DTCM_ITEM_MAX];

