set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall -O0 --coverage")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -Wall -O0 --coverage")

add_definitions(-DDT_TEST)

add_subdirectory(third/mockcpp)
target_compile_options(mockcpp PRIVATE -std=c++14)

add_subdirectory(third/googletest)
target_compile_options(gtest PRIVATE -std=c++14)
target_compile_options(gtest_main PRIVATE -std=c++14)

set(DT_TEST_DIR ${CMAKE_CURRENT_SOURCE_DIR})

set(DT_TEST_MAIN ${DT_TEST_DIR}/main.cpp)
file(GLOB_RECURSE APP_FILE_DT ${DT_TEST_DIR}/app/*.c ${DT_TEST_DIR}/app/*.cpp)
file(GLOB_RECURSE HAL_FILE_DT ${DT_TEST_DIR}/hal/*.c ${DT_TEST_DIR}/hal/*.cpp)
file(GLOB_RECURSE RAID_FILE_DT ${DT_TEST_DIR}/raid/*.c ${DT_TEST_DIR}/raid/*.cpp)

list(APPEND CODING_TEST_SRCS_DT ${DT_TEST_MAIN}
                                ${APP_FILE_DT}
                                ${HAL_FILE_DT}
                                ${DFX_FILE}
                                ${RAID_FILE_DT}
                                )

set(CODING_TEST_TARGET_DT "coding_test_dt")
add_executable(${CODING_TEST_TARGET_DT} ${CODING_TEST_SRCS_DT} ${CODING_TEST_SRCS_FOR_DT})

target_include_directories( ${CODING_TEST_TARGET_DT}
                            PUBLIC
                            ${PRJ_H_FOLDERS} ${SRC_H_FOLDERS}
							${DT_TEST_DIR}/third/googletest/googletest/include
							${DT_TEST_DIR}/third/mockcpp/include
							${DT_TEST_DIR}/third/mockcpp/include/mockcpp
							${DT_TEST_DIR}/third/mockcpp/3rdparty
							${DT_TEST_DIR}/share/include
                            )
target_link_libraries(${CODING_TEST_TARGET_DT} PUBLIC mockcpp gtest gtest_main)
target_compile_options(${CODING_TEST_TARGET_DT} PRIVATE -g -Wall -O0 --coverage)
