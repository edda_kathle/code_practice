
#include <iostream>

#include "gtest/gtest.h"
#include <mockcpp/mokc.h>

using namespace std;

class CodingTestDT : public testing::Environment
{
    public:
        virtual void SetUp()
        {
            cout << "Global event: start" << endl;
        }

        virtual void TearDown()
        {
            cout << "Global event: end" << endl;
        }
};

int main(int argc, char *argv[])
{
    int16_t a = 3;

    a--;

    testing::AddGlobalTestEnvironment(new CodingTestDT);
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}

