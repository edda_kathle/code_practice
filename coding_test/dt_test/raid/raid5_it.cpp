#include <stdio.h>
#include <stdlib.h>

#include <mockcpp/mokc.h>
#include "gtest/gtest.h"

#include "c_defines.h"
#include "raid5.h"

class raid5_it : public testing::Test
{
protected:
    void SetUp() override
    {
    }

    void TearDown() override
    {
        GlobalMockObject::verify();
    }
};

TEST_F(raid5_it, run_raid5_sample_code)
{
    int32_t ret = run_raid5_sample_code();
    EXPECT_EQ(ret, 0);
}

TEST_F(raid5_it, xor_8regs_2)
{
#define RAID_BUFF_LEN 128
#define RAID_SRC_DATA 0xAA
    unsigned long *p1 = (unsigned long *)malloc(RAID_BUFF_LEN);
    unsigned long *p2 = (unsigned long *)malloc(RAID_BUFF_LEN);

    memset(p1, RAID_SRC_DATA, RAID_BUFF_LEN);
    memset(p2, ~RAID_SRC_DATA, RAID_BUFF_LEN);

    xor_8regs_2(RAID_BUFF_LEN, p1, p2);
}
