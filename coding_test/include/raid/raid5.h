#ifndef _RAID5_H_
#define _RAID5_H_

#include <stdint.h>
#include "c_defines.h"

BEGIN_DECLS

int32_t run_raid5_sample_code(void);
void xor_8regs_2(unsigned long bytes, unsigned long *p1, unsigned long *p2);

END_DECLS

#endif
