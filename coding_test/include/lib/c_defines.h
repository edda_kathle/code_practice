#ifndef _CDEFINES_H
#define _CDEFINES_H

// for gtest&mockcpp test,need add extern "C" in headfiles
// BEGIN_DECLS/END_DECLS is simple version
#ifdef __cplusplus
    #define BEGIN_DECLS extern "C" {
    #define END_DECLS   }
#else
    #define BEGIN_DECLS
    #define END_DECLS
#endif

#ifdef DT_TEST
    #define __DDR_DATA__
    #define __DDR_BSS__
    #define __ALIGNED16__
    #define __ALIGNED4096__
    #define __DDR_CODE__
#else
    #define __DDR_DATA__ __attribute__((section(".ddr_data")))
    #define __DDR_BSS__ __attribute__((section(".bss")))
    #define __DDR_FW__ __attribute__((section(".ddr_fw")))
    #define __ALIGNED16__ __attribute__((aligned(16)))
    #define __ALIGNED4096__ __attribute__((aligned(4096)))
    #define __DDR_CODE__ __attribute__((section(".ddr_code")))
#endif

#endif
