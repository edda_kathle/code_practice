// https://blog.csdn.net/m0_59416558/article/details/127453236

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <asm/io.h>

// #define GPX1CON 0x001fffff // 0x11000c20
// #define GPX1DAT 0x00200004 //0x11000c24

#define LED_REG_SPACE_SIZE 4096
void *GPX1CON_VIRT = NULL;
void *GPX1DAT_VIRT = NULL;
void *GPX1CON = NULL;
void *GPX1DAT = NULL;

int malloc_dma_mem_for_led_reg(void)
{
		GPX1CON_VIRT = kmalloc(LED_REG_SPACE_SIZE, GFP_ATOMIC);
		if (!GPX1CON_VIRT) {
			printk("kmalloc GPX1CON_VIRT failed.\n");
			return -1;
		}
		GPX1CON = virt_to_phys(GPX1CON_VIRT);

		GPX1DAT_VIRT = kmalloc(LED_REG_SPACE_SIZE, GFP_ATOMIC);
		if (!GPX1DAT_VIRT) {
			kfree(GPX1CON_VIRT);
			printk("kmalloc GPX1DAT_VIRT failed.\n");
			return -1;
		}
		GPX1DAT = virt_to_phys(GPX1DAT_VIRT);

		printk("kmalloc ok(%#x %#x %#x %#x).\n", GPX1CON_VIRT, GPX1CON, GPX1DAT_VIRT, GPX1DAT);

		return 0;
}

void free_led_reg_mem(void)
{
	kfree(GPX1CON_VIRT);
	kfree(GPX1DAT_VIRT);
	return;
}

//设计结构体类型来表示整个驱动对象(描述设备驱动信息)
struct led_drv
{
		unsigned int major;//主设备号
		dev_t devno;//设备号
		struct class * cls;//设备文件信息结构体
		struct device* dev;//设备文件信息
		unsigned int * gpx1con;//设备驱动映射的地址
		unsigned int * gpx1dat;
};
//定义全局设备对象(存储信息)
struct led_drv  led;

//驱动与应用程序函数关联
int led_open(struct inode * inode, struct file * file)
{
	printk("hqb led_open\n");
	return 0;
}

int led_close(struct inode * inode, struct file * file)
{
	printk("hqb led_close\n");
	return 0;
}

ssize_t led_write(struct file * file, const char __user * buf, size_t size, loff_t * ops)
{
	int num = 0;
	void *p = copy_from_user(&num, buf, size);
	printk("addr is = %p num:%#x size:%d.\n", p, num, size);

	dump_stack();

	if (num == 1)
	{
		*(led.gpx1dat) |= 1;
	}
	else
	{
		*(led.gpx1dat) &= ~1;
	}

	return 0;
}

const struct file_operations fops = {
	.open = led_open,
	.release = led_close,
	.write = led_write,
};

static int __init led_init(void)
{
	//申请设备号
	led.major = 100; //250; wsl中设备号250已经被其它设备占用
	led.devno = led.major << 20 | 0;
	int ret = -1;
	ret = register_chrdev(led.major, "led drv", &fops);
	if (ret < 0)
	{
		printk("register devno error\n");
		goto err_1;
	}

	//创建设备文件
	led.cls = class_create(THIS_MODULE, "led cls");
	if (IS_ERR(led.cls))
	{
		printk("class create error\n");
		goto err_2;
	}

	led.dev = device_create(led.cls, NULL, led.devno, NULL, "led");
	if (IS_ERR(led.dev))
	{
		printk("device create erroe\n");
		goto err_3;
	}

	if (malloc_dma_mem_for_led_reg() != 0) {
		printk("kmalloc failed.\n");
		goto err_4;
	}

	//硬件初始化
	led.gpx1con = ioremap(GPX1CON, 4);
	if (led.gpx1con == NULL)
	{
		printk("gpx1con ioremap error\n");
		goto err_4;
	}

	led.gpx1dat = ioremap(GPX1DAT, 4);
	if (led.gpx1dat == NULL)
	{
		printk("gpx1dat ioremap error\n");
		goto err_5;
	}

	//硬件寄存器初始化
	*(led.gpx1con) = *(led.gpx1con) & ~(0xf) | 0x1;
	*(led.gpx1dat) |= 1;

	return 0;

err_5:
	iounmap(led.gpx1con);

err_4:
	device_destroy(led.cls, led.devno);

err_3:
	class_destroy(led.cls);

err_2:
	unregister_chrdev(led.major, "led drv");

err_1:
	return -1;
}

static void __exit led_exit(void)
{
	//卸载驱动内容
	//1、释放映射地址
	iounmap(led.gpx1con);
	iounmap(led.gpx1dat);

	//2、释放设备文件
	device_destroy(led.cls,led.devno);
	//3、释放设备文件结构体
	class_destroy(led.cls);

	//4、释放设备号
	unregister_chrdev(led.major,"led drv");

	free_led_reg_mem();
}

module_init(led_init);
module_exit(led_exit);
MODULE_LICENSE("GPL");
