#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main()
{
	int fd = open("/dev/led",O_WRONLY);
	int i = 0;
	int a;

	while(i < 2)
	{
		a = 0x5555AAAA;
		write(fd, &a, 4);
		sleep(1);

		a = 0xAAAA5555;
		write(fd, &a, 4);
		sleep(1);

		i++;
	}

	close(fd);

	return 0;
}
